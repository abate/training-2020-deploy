# Simple ansible deployment for tezos

    $ ansible-galaxy install -r requirements.yml

    $ ansible-playbook site.yml -K


## Init the machine

    $ ansible-playbook site.yml -K -t install

## Init the node by using a snapshot

A special tag `remove_data_volume` can be used to restart from scratch

    $ ansible-playbook site.yml -K -t tezos-babylonnet-init

# Start the node

    $ ansible-playbook site.yml -K -t tezos-babylonnet-node

# Start the baker accuser and endorser

    $ ansible-playbook site.yml -K -t tezos-babylonnet-baker

establish a ssh tunnel to the machine

```
Host baker
  HostName 35.181.154.189
  User ubuntu
  RemoteForward 172.17.0.1:6732 127.0.0.1:6732
  LocalForward 18732 172.17.0.1:8732
```

We forward the rpc port of the node and the signer port
